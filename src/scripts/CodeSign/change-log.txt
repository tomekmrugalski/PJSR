===============================================================================
PixInsight Code Signing Utility Script Changelog
===============================================================================

2024/03/12 - v1.1

- The script can now generate signatures for module binary files on all
  supported platforms (*.so, *.dylib, *.dll). This feature was added in
  PixInsight core 1.8.9-2 build 1602.

- PixInsight core version >= 1.8.9-2 build 1602 required.

-------------------------------------------------------------------------------
2022/04/13 - v1.0

- Initial version released with PixInsight core 1.8.9-1
-------------------------------------------------------------------------------
