===============================================================================
Ephemerides Script Changelog
===============================================================================

2024.05.06 - v1.20

   - Added support for core KBO ephemerides.

   - Added support for comet total and nuclear visual magnitudes.

   - Astrometric positions are now computed by default (previously apparent
     coordinates).

   - Local apparent horizontal coordinates (azimuth and altitude) are now
     included in topocentric ephemerides.

   - Improved sorting criteria for generation of combo box object lists from
     custom XEPH files.

   - Script parameters are now persistent across executions through Settings.

   - Core version 1.8.9-2 build 1606 or higher required.

-------------------------------------------------------------------------------
2021.03.27 - v1.10

   - Added support for custom XEPH files.

   - Added #feature-icon directive.

   - Core version 1.8.8-7 or higher required.

   - Released with PixInsight core 1.8.8-8.

-------------------------------------------------------------------------------
2020.10.07 - v1.02

   - Bugfix: Wrong representation of star right ascension coordinates as
     degrees instead of hours on the main Ephemeris dialog.

-------------------------------------------------------------------------------
2019.04.29 - v1.01

   - Minor fix: The initial time is now initialized to the current day at 0h
     UTC by default (previously it was January 1.0 of the next year).

-------------------------------------------------------------------------------
2018.11.28 - v1.0

   - First public version. Released with PixInsight core 1.8.6.
