/*
 * Warp Image
 *
 * This file is part of the MosaicByCoordinates and AlignByCoordinates scripts.
 *
 * Copyright (C) 2014-2024, Andres del Pozo
 * Copyright (C) 2019-2024, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

function WarpImage( pixelInterpolation, clampingThreshold, suffix )
{
   this.pixelInterpolation = pixelInterpolation;
   this.clampingThreshold = clampingThreshold;
   this.suffix = suffix;

   this.reprojectedImage = function( referenceMetadata, sourceWindow )
   {
      let outputWindow;
      try
      {
         if ( !referenceMetadata.projection )
            throw "The reference image has no astrometric solution";
         if ( !sourceWindow.hasAstrometricSolution )
            throw "The source image has no astrometric solution";

         let T = new ElapsedTime;

         let sourceImage = sourceWindow.mainView.image;
         sourceImage.interpolation = this.pixelInterpolation;
         sourceImage.interpolationClamping = this.clampingThreshold;

         outputWindow = new ImageWindow( referenceMetadata.width, referenceMetadata.height,
                                         sourceImage.numberOfChannels, 32/*bitsPerSample*/, true/*isReal*/, sourceImage.isColor,
                                         sourceWindow.mainView.id + this.suffix );

         outputWindow.mainView.beginProcess( UndoFlag_NoSwapFile );

         outputWindow.keywords = sourceWindow.keywords;
         referenceMetadata.SaveKeywords( outputWindow, false/*beginProcess*/ );
         referenceMetadata.SaveProperties( outputWindow );

         outputWindow.regenerateAstrometricSolution();

         let image = outputWindow.mainView.image;
         image.statusEnabled = true;
         image.statusInitializationEnabled = true;
         image.interpolationQuality = 1;
         outputWindow.astrometricReprojection( sourceWindow );

         outputWindow.mainView.endProcess();

         console.writeln( "<end><cbr>" + T.text );
         console.flush();

         return outputWindow;
      }
      catch ( ex )
      {
         console.writeln( ex );
         if ( outputWindow )
            outputWindow.forceClose();
         return null;
      }
   };
}

/*
 * Returns the bounding rectangle in pixels of the image of sourceMetadata
 * after projecting it to the geometry of referenceMetadata.
 */
WarpImage.reprojectedBounds = function( referenceMetadata, sourceMetadata )
{
   let nx = Math.min( 100, sourceMetadata.width );
   let ny = Math.min( 100, sourceMetadata.height );
   let bounds = null;
   for ( let yi = 0; yi <= ny; ++yi )
   {
      let y = yi * sourceMetadata.height / ny;
      for ( let xi = 0; xi <= nx; ++xi )
      {
         let x = xi * sourceMetadata.width / nx;
         let pRD = sourceMetadata.Convert_I_RD( new Point( x, y ) );
         if ( pRD && isFinite( pRD.x ) && isFinite( pRD.y ) )
         {
            let pPx0 = referenceMetadata.Convert_RD_I( pRD );
            if ( pPx0 && isFinite( pPx0.x ) && isFinite( pPx0.y ) )
               if ( bounds == null )
                  bounds = new Rect( pPx0.x, pPx0.y, pPx0.x, pPx0.y );
               else
               {
                  bounds.left = Math.min( bounds.left, pPx0.x );
                  bounds.top = Math.min( bounds.top, pPx0.y );
                  bounds.right = Math.max( bounds.right, pPx0.x );
                  bounds.bottom = Math.max( bounds.bottom, pPx0.y );
               }
         }
      }
   }
   return bounds;
};
