===============================================================================
AperturePhotometry Script Changelog
===============================================================================

2024/02/24 - v1.5.3

- Bugfix: Downloaded VizieR data is stored in a different plain text file for
  each running instance of the core application. Original bug report:
  https://pixinsight.com/forum/index.php?threads/
      imagesolver-cannot-be-run-from-multiple-instances-
      alignbycoordinates-name-part-gets-duplicated.22690/

- PixInsight core version >= 1.8.9-2 required.

-------------------------------------------------------------------------------
2021/04/11 - v1.5.2

- PixInsight core version >= 1.8.8-8 required as a result of new astrometric
  solutions with selectable reference system support.

-------------------------------------------------------------------------------
2021/01/17 - v1.5.1

- Script icon created.

-------------------------------------------------------------------------------
2020/12/11 - v1.5.0

- New APASS DR9 and DR10 catalogs based on XPSD local databases and the APASS
  XPSD server process.

- New autoMagnitude engine parameter. When this parameter is true and an XPSD
  server catalog is selected (such as an APASS or Gaia XPSD catalog), compute
  an optimal limit magnitude automatically using fast XPSD server search
  operations. In such cases the maxMagnitude parameter will be ignored.

- Redesigned/refactored script dialog. Fixed all dialog layout problems.

-------------------------------------------------------------------------------
v1.4.7

- Fix dialog layout problems on KDE Plasma 5 generated in
  SectionBar.onToggleSection() event handlers.

-------------------------------------------------------------------------------
v1.4.6

- Use a wider aperture when searching for stars in high resolution images.

- Fixed problem with stars too close to an edge of the image.

-------------------------------------------------------------------------------
v1.4.5

- Removed obsolete check when writing the result file which produced the
  warning "Invalid out format".

-------------------------------------------------------------------------------
v1.4.4

- Fixed null pointer (https://pixinsight.com/forum/index.php?topic=11982).

-------------------------------------------------------------------------------
v1.4.3

- Better error management in the online catalogs.

-------------------------------------------------------------------------------
v1.4.2

- Fixed the photometry with small apertures.

-------------------------------------------------------------------------------
v1.4.1

- Added resetSettings and resetSettingsAndExit script parameters for
  reinitialization from PCL hybrid modules.

-------------------------------------------------------------------------------
v1.4

- Added selection of the units of the aperture: pixels or arcseconds.

- Added support and warning for apertures less that 2 pixels.

-------------------------------------------------------------------------------
v1.3.1

- Added generation of global control variables for invocation from PCL-based
  modules.

- Require PixInsight core version 1.8.4.

- Fixed release year.

- Improved some text messages and labels.

-------------------------------------------------------------------------------
v1.3

- Changes by Colin McGill: new PSF_FLUX table and write all the available
  magnitudes and the airmass in all the tables.

-------------------------------------------------------------------------------
v1.2.3

- Option for choosing in the solver between using the image metadata or the
  configuration of the solver dialog.

-------------------------------------------------------------------------------
v1.2.2

- Added support for XISF files.

-------------------------------------------------------------------------------
v1.2.1

- Fixed button "Browse output directory".

-------------------------------------------------------------------------------
v1.2

- The script now measures all the stars in an area 50% bigger than the field
  of view of the first image.

- The queries to the catalog are now more efficient and the cache is kept
  between executions.

- Option for saving the diagnostic images.

- Configuration of the prefix of the name of the output tables.

- New option "non-interactive" for non-interactive execution.

-------------------------------------------------------------------------------
v1.1.1

- Changes required by changes in ImageSolver.

-------------------------------------------------------------------------------
v1.1

- Extract stars directly from the catalog.

- Force solve image.

- User defined objects.

- Use of new control for collapsible panels.

- Several Spectrophotometric Catalogs:
      III/126 Burnashev
      II/183A Landolt
      III/201 Pulkovo
      III/202 Kharitonov
      J/A+AS/92/1 Glushneva+

-------------------------------------------------------------------------------
v1.0

- Initial version.
