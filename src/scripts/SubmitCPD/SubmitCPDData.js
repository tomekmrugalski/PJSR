// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// SubmitCPDData.js - Released 2022-04-13T18:28:34Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight CPD Submission Utility Script 1.0
//
// Copyright (c) 2021-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight CPD Submission Utility
 *
 * Copyright (C) 2021-2022 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Working Data.
 */

#include <pjsr/DataType.jsh>

function SubmitCPDWorkingData()
{
   this.__base__ = Object;
   this.__base__();

   this.useKeysFile = true;
   this.keysFile = "";
   this.developerId = "";
   this.publicKeyHex = "";
   this.contactEmail = "";
   this.publicEmail = "";
   this.url = "";
   this.name = "";
   this.info = "";

   this.saveSettings = function()
   {
      function save( key, type, value )
      {
         Settings.write( SETTINGS_KEY_BASE + key, type, value );
      }

      save( "useKeysFile", DataType_Boolean, this.useKeysFile );
      save( "keysFile", DataType_String, this.keysFile );
      save( "developerId", DataType_String, this.developerId );
      save( "contactEmail", DataType_String, this.contactEmail );
      save( "publicEmail", DataType_String, this.publicEmail );
      save( "url", DataType_String, this.url );
      save( "name", DataType_String, this.name );
      save( "info", DataType_String, this.info );
   };

   this.loadSettings = function()
   {
      function load( key, type )
      {
         return Settings.read( SETTINGS_KEY_BASE + key, type );
      }

      let o = load( "useKeysFile", DataType_Boolean );
      if ( o != null )
         this.useKeysFile = o;

      o = load( "keysFile", DataType_String );
      if ( o != null )
         this.keysFile = o;

      o = load( "developerId", DataType_String );
      if ( o != null )
         this.developerId = o;

      o = load( "contactEmail", DataType_String );
      if ( o != null )
         this.contactEmail = o;

      o = load( "publicEmail", DataType_String );
      if ( o != null )
         this.publicEmail = o;

      o = load( "url", DataType_String );
      if ( o != null )
         this.url = o;

      o = load( "name", DataType_String );
      if ( o != null )
         this.name = o;

      o = load( "info", DataType_String );
      if ( o != null )
         this.info = o;
   };

   this.reset = function()
   {
      this.useKeysFile = true;
      this.keysFile = "";
      this.developerId = "";
      this.publicKeyHex = "";
      this.contactEmail = "";
      this.publicEmail = "";
      this.url = "";
      this.name = "";
      this.info = "";
   };
}

SubmitCPDWorkingData.prototype = new Object;

/*
 * Global working data object
 */
var g_workingData = new SubmitCPDWorkingData;

// ----------------------------------------------------------------------------
// EOF SubmitCPDData.js - Released 2022-04-13T18:28:34Z
