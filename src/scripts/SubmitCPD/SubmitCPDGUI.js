// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// SubmitCPDGUI.js - Released 2022-04-13T18:28:34Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight CPD Submission Utility Script 1.0
//
// Copyright (c) 2021-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight CPD Submission Utility
 *
 * Copyright (C) 2021-2022 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Graphical user interface
 */

#include <pjsr/FocusStyle.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/TextAlign.jsh>

/*
 * CPD Data Submission Dialog
 */
function SubmitCPDDialog()
{
   this.__base__ = Dialog;
   this.__base__();

   //

   let emWidth = this.font.width( 'M' );
   let labelWidth1 = this.font.width( "Developer identifier:" ) + emWidth;
   let editWidth1 = 40*emWidth;

   // https://stackoverflow.com/questions/201323/how-can-i-validate-an-email-address-using-a-regular-expression
   let rx_rfc5322 = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

   // https://gist.github.com/dperini/729294
//    let rx_url = "(?:(?:https?|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\.254(?:\\.\\d{1,3}){2})(?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)*(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}]{2,})))(?::\\d{2,5})?(?:/[^\\s]*)?";

   //

   this.helpLabel = new Label( this );
   this.helpLabel.styleSheet = this.scaledStyleSheet(
      "QWidget#" + this.helpLabel.uniqueId + " {"
   +     "border: 1px solid gray;"
   +     "padding: 0.25em;"
   +  "}" );
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<p><strong>" + TITLE + " version " + VERSION + "</strong><br/>" +
                         "Copyright &copy; 2021-2022 Pleiades Astrophoto. All Rights Reserved.</p>";
   //

   this.useKeysFile_CheckBox = new CheckBox( this );
   this.useKeysFile_CheckBox.text = "Use keys file";
   this.useKeysFile_CheckBox.toolTip =
      "<p>Use an existing signing keys file to retrieve your developer identifier and public signing key.</p>";
   this.useKeysFile_CheckBox.onClick = function( checked )
   {
      g_workingData.useKeysFile = checked;
      this.dialog.updateControls();
   };

   this.useKeysFile_Sizer = new HorizontalSizer;
   this.useKeysFile_Sizer.addUnscaledSpacing( labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.useKeysFile_Sizer.add( this.useKeysFile_CheckBox );
   this.useKeysFile_Sizer.addStretch();

   //

   let keysFile_ToolTip = "<p>The signing keys file with the developer identifier and public signing " +
      "key that will be submitted.</p>" +
      "<p>Note: Only your public signing key will be read from the specified signing keys file, " +
      "and then it will be decrypted using the password entered in the corresponding field below. " +
      "The private signing key won't even be read from the .xssk file.</p>";

   this.keysFile_Label = new Label( this );
   this.keysFile_Label.text = "Keys file:";
   this.keysFile_Label.minWidth = labelWidth1;
   this.keysFile_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.keysFile_Label.toolTip = keysFile_ToolTip;

   this.keysFile_Edit = new Edit( this );
   this.keysFile_Edit.minWidth = editWidth1;
   this.keysFile_Edit.toolTip = keysFile_ToolTip;
   this.keysFile_Edit.onEditCompleted = function()
   {
      let filePath = File.windowsPathToUnix( this.text.trim() );
      this.text = filePath;
      g_workingData.keysFile = filePath;
   };

   this.keysFileSelect_Button = new ToolButton( this );
   this.keysFileSelect_Button.icon = this.scaledResource( ":/icons/select-file.png" );
   this.keysFileSelect_Button.setScaledFixedSize( 20, 20 );
   this.keysFileSelect_Button.toolTip = "<p>Select the signing keys file.</p>";
   this.keysFileSelect_Button.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = false;
      ofd.caption = "Select Signing Keys File";
      ofd.filters = [ ["Secure signing keys files", "*.xssk"] ];
      if ( ofd.execute() )
         this.dialog.keysFile_Edit.text = g_workingData.keysFile = ofd.fileNames[0];
   };

   this.keysFile_Sizer = new HorizontalSizer;
   this.keysFile_Sizer.spacing = 4;
   this.keysFile_Sizer.add( this.keysFile_Label );
   this.keysFile_Sizer.add( this.keysFile_Edit, 100 );
   this.keysFile_Sizer.add( this.keysFileSelect_Button );

   //

   let password_ToolTip = "<p>Secure signing keys files (.xssk) are password-protected. " +
      "Enter here the password corresponding to the keys file that you have specified above.</p>" +
      "<p>Note: Only your public signing key will be read from the specified signing keys file, " +
      "and then it will be decrypted using the password entered in this field. The private " +
      "signing key won't even be read from the .xssk file.</p>";

   this.password_Label = new Label( this );
   this.password_Label.text = "Password:";
   this.password_Label.minWidth = labelWidth1;
   this.password_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.password_Label.toolTip = password_ToolTip;

   this.password_Edit = new Edit( this );
   this.password_Edit.passwordMode = true;
   this.password_Edit.setScaledFixedWidth( 20*emWidth );
   this.password_Edit.toolTip = password_ToolTip;
   this.password_Edit.onEditCompleted = function()
   {
      this.text = this.text.trim();
   };

   this.passwordHide_Button = new ToolButton( this );
   this.passwordHide_Button.checkable = true;
   this.passwordHide_Button.checked = true;
   this.passwordHide_Button.icon = this.scaledResource( ":/icons/control-password.png" );
   this.passwordHide_Button.setScaledFixedSize( 22, 22 );
   this.passwordHide_Button.focusStyle = FocusStyle_NoFocus;
   this.passwordHide_Button.toolTip = "<p>Hide the password.</p>";
   this.passwordHide_Button.onClick = function( checked )
   {
      this.dialog.password_Edit.passwordMode = checked;
   };

   this.passwordClear_Button = new ToolButton( this );
   this.passwordClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.passwordClear_Button.setScaledFixedSize( 22, 22 );
   this.passwordClear_Button.focusStyle = FocusStyle_NoFocus;
   this.passwordClear_Button.toolTip = "<p>Clear the Password field.</p>";
   this.passwordClear_Button.onClick = function()
   {
      this.dialog.password_Edit.text = "";
      this.dialog.password_Edit.hasFocus = true;
   };

   this.password_Sizer = new HorizontalSizer;
   this.password_Sizer.spacing = 4;
   this.password_Sizer.add( this.password_Label );
   this.password_Sizer.add( this.password_Edit, 100 );
   this.password_Sizer.add( this.passwordHide_Button );
   this.password_Sizer.add( this.passwordClear_Button );
   this.password_Sizer.addStretch();

   //

   this.useManualData_CheckBox = new CheckBox( this );
   this.useManualData_CheckBox.text = "Enter id/key data";
   this.useManualData_CheckBox.toolTip =
      "<p>Enter your developer identifier and public signing key manually.</p>" +
      "<p>Use this option if you don't trust us to retrieve your public key from " +
      "a signing keys file.</p>";
   this.useManualData_CheckBox.onClick = function( checked )
   {
      g_workingData.useKeysFile = !checked;
      this.dialog.updateControls();
   };

   this.useManualData_Sizer = new HorizontalSizer;
   this.useManualData_Sizer.addUnscaledSpacing( labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.useManualData_Sizer.add( this.useManualData_CheckBox );
   this.useManualData_Sizer.addStretch();

   //

   let developerId_ToolTip = "<p>Enter your developer identifier.</p>";

   this.developerId_Label = new Label( this );
   this.developerId_Label.text = "Developer identifier:";
   this.developerId_Label.minWidth = labelWidth1;
   this.developerId_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.developerId_Label.toolTip = developerId_ToolTip;

   this.developerId_Edit = new Edit( this );
   this.developerId_Edit.toolTip = developerId_ToolTip;
   this.developerId_Edit.onEditCompleted = function()
   {
      g_workingData.developerId = this.text = this.text.trim();
   };

   this.developerIdClear_Button = new ToolButton( this );
   this.developerIdClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.developerIdClear_Button.setScaledFixedSize( 22, 22 );
   this.developerIdClear_Button.focusStyle = FocusStyle_NoFocus;
   this.developerIdClear_Button.toolTip = "<p>Clear the Password field.</p>";
   this.developerIdClear_Button.onClick = function()
   {
      this.dialog.developerId_Edit.text = "";
      this.dialog.developerId_Edit.hasFocus = true;
   };

   this.developerId_Sizer = new HorizontalSizer;
   this.developerId_Sizer.spacing = 4;
   this.developerId_Sizer.add( this.developerId_Label );
   this.developerId_Sizer.add( this.developerId_Edit, 100 );
   this.developerId_Sizer.add( this.developerIdClear_Button );

   //

   let publicKeyHex_ToolTip = "<p>Enter your public key in hexadecimal format.</p>" +
      "<p>A public signing key has 32 bytes, which in hexadecimal format requires 64 characters. " +
      "You can get your public key with the <i>Security.loadSigningKeysFile()</i> function.</p>";

   this.publicKeyHex_Label = new Label( this );
   this.publicKeyHex_Label.text = "Public key (hex):";
   this.publicKeyHex_Label.minWidth = labelWidth1;
   this.publicKeyHex_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.publicKeyHex_Label.toolTip = publicKeyHex_ToolTip;

   this.publicKeyHex_Edit = new Edit( this );
   this.publicKeyHex_Edit.validatingRegExp = "[0-9a-fA-F]{64,64}";
   this.publicKeyHex_Edit.toolTip = publicKeyHex_ToolTip;
   this.publicKeyHex_Edit.onEditCompleted = function()
   {
      g_workingData.publicKeyHex = this.text = this.text.trim();
   };

   this.publicKeyHexClear_Button = new ToolButton( this );
   this.publicKeyHexClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.publicKeyHexClear_Button.setScaledFixedSize( 22, 22 );
   this.publicKeyHexClear_Button.focusStyle = FocusStyle_NoFocus;
   this.publicKeyHexClear_Button.toolTip = "<p>Clear the Password field.</p>";
   this.publicKeyHexClear_Button.onClick = function()
   {
      this.dialog.publicKeyHex_Edit.text = "";
      this.dialog.publicKeyHex_Edit.hasFocus = true;
   };

   this.publicKeyHex_Sizer = new HorizontalSizer;
   this.publicKeyHex_Sizer.spacing = 4;
   this.publicKeyHex_Sizer.add( this.publicKeyHex_Label );
   this.publicKeyHex_Sizer.add( this.publicKeyHex_Edit, 100 );
   this.publicKeyHex_Sizer.add( this.publicKeyHexClear_Button );

   //

   let contactEmail_ToolTip = "<p>Your contact email address (required).</p>" +
      "<p>If necessary, we'll use this email address to contact you in relation " +
      "to your CPD data submission.</p>" +
      "<p>This email address will be kept private; it will never be distributed " +
      "with the PixInsight core application.</p>";

   this.contactEmail_Label = new Label( this );
   this.contactEmail_Label.text = "Contact email:";
   this.contactEmail_Label.minWidth = labelWidth1;
   this.contactEmail_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.contactEmail_Label.toolTip = contactEmail_ToolTip;

   this.contactEmail_Edit = new Edit( this );
   this.contactEmail_Edit.validatingRegExp = rx_rfc5322;
   this.contactEmail_Edit.toolTip = contactEmail_ToolTip;
   this.contactEmail_Edit.onEditCompleted = function()
   {
      g_workingData.contactEmail = this.text = this.text.trim();
   };

   this.contactEmailClear_Button = new ToolButton( this );
   this.contactEmailClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.contactEmailClear_Button.setScaledFixedSize( 22, 22 );
   this.contactEmailClear_Button.focusStyle = FocusStyle_NoFocus;
   this.contactEmailClear_Button.toolTip = "<p>Clear the contact email address field.</p>";
   this.contactEmailClear_Button.onClick = function()
   {
      g_workingData.contactEmail = this.dialog.contactEmail_Edit.text = "";
      this.dialog.contactEmail_Edit.hasFocus = true;
   };

   this.contactEmail_Sizer = new HorizontalSizer;
   this.contactEmail_Sizer.spacing = 4;
   this.contactEmail_Sizer.add( this.contactEmail_Label );
   this.contactEmail_Sizer.add( this.contactEmail_Edit, 100 );
   this.contactEmail_Sizer.add( this.contactEmailClear_Button );

   //

   let publicEmail_ToolTip = "<p>Your public email address (optional).</p>" +
      "<p>This email address, if specified, will be included in the CPD database " +
      "distributed with the PixInsight core application.</p>";

   this.publicEmail_Label = new Label( this );
   this.publicEmail_Label.text = "Public email:";
   this.publicEmail_Label.minWidth = labelWidth1;
   this.publicEmail_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.publicEmail_Label.toolTip = publicEmail_ToolTip;

   this.publicEmail_Edit = new Edit( this );
   this.publicEmail_Edit.validatingRegExp = rx_rfc5322;
   this.publicEmail_Edit.toolTip = publicEmail_ToolTip;
   this.publicEmail_Edit.onEditCompleted = function()
   {
      g_workingData.publicEmail = this.text = this.text.trim();
   };

   this.publicEmailClear_Button = new ToolButton( this );
   this.publicEmailClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.publicEmailClear_Button.setScaledFixedSize( 22, 22 );
   this.publicEmailClear_Button.focusStyle = FocusStyle_NoFocus;
   this.publicEmailClear_Button.toolTip = "<p>Clear the public email address field.</p>";
   this.publicEmailClear_Button.onClick = function()
   {
      g_workingData.publicEmail = this.dialog.publicEmail_Edit.text = "";
      this.dialog.publicEmail_Edit.hasFocus = true;
   };

   this.publicEmail_Sizer = new HorizontalSizer;
   this.publicEmail_Sizer.spacing = 4;
   this.publicEmail_Sizer.add( this.publicEmail_Label );
   this.publicEmail_Sizer.add( this.publicEmail_Edit, 100 );
   this.publicEmail_Sizer.add( this.publicEmailClear_Button );

   //

   let url_ToolTip = "<p>The URL where you provide information about your development work " +
      "(optional, but highly recommended).</p>";

   this.url_Label = new Label( this );
   this.url_Label.text = "URL:";
   this.url_Label.minWidth = labelWidth1;
   this.url_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.url_Label.toolTip = url_ToolTip;

   this.url_Edit = new Edit( this );
   this.url_Edit.toolTip = url_ToolTip;
   this.url_Edit.onEditCompleted = function()
   {
      g_workingData.url = this.text = this.text.trim();
   };

   this.urlClear_Button = new ToolButton( this );
   this.urlClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.urlClear_Button.setScaledFixedSize( 22, 22 );
   this.urlClear_Button.focusStyle = FocusStyle_NoFocus;
   this.urlClear_Button.toolTip = "<p>Clear the URL field.</p>";
   this.urlClear_Button.onClick = function()
   {
      g_workingData.url = this.dialog.url_Edit.text = "";
      this.dialog.url_Edit.hasFocus = true;
   };

   this.url_Sizer = new HorizontalSizer;
   this.url_Sizer.spacing = 4;
   this.url_Sizer.add( this.url_Label );
   this.url_Sizer.add( this.url_Edit, 100 );
   this.url_Sizer.add( this.urlClear_Button );

   //

   let name_ToolTip = "<p>Your name, or the name of your company, so your users can identify you " +
      "(optional, but highly recommended). If desired, you can enter multiple text lines in this field.</p>";

   this.name_Label = new Label( this );
   this.name_Label.text = "Name:";
   this.name_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.name_Label.minWidth = labelWidth1 - this.logicalPixelsToPhysical( 1 );
   this.name_Label.setScaledFixedHeight( 50 );
   this.name_Label.toolTip = name_ToolTip;

   this.name_TextBox = new TextBox( this );
   this.name_TextBox.toolTip = name_ToolTip;
   this.name_TextBox.setScaledFixedHeight( 50 );

   this.nameClear_Button = new ToolButton( this );
   this.nameClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.nameClear_Button.setScaledFixedSize( 22, 22 );
   this.nameClear_Button.focusStyle = FocusStyle_NoFocus;
   this.nameClear_Button.toolTip = "<p>Clear the Name field.</p>";
   this.nameClear_Button.onClick = function()
   {
      g_workingData.name = this.dialog.name_TextBox.text = "";
      this.dialog.name_TextBox.hasFocus = true;
   };

   this.name_Sizer = new HorizontalSizer;
   this.name_Sizer.spacing = 4;
   this.name_Sizer.add( this.name_Label );
   this.name_Sizer.add( this.name_TextBox, 100 );
   this.name_Sizer.add( this.nameClear_Button );

   //

   let info_ToolTip = "<p>Provide additional information about you and your work (optional). " +
      "If desired, you can enter multiple text lines in this field.</p>";

   this.info_Label = new Label( this );
   this.info_Label.text = "Information:";
   this.info_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.info_Label.minWidth = labelWidth1 - this.logicalPixelsToPhysical( 1 );
   this.info_Label.setScaledFixedHeight( 80 );
   this.info_Label.toolTip = info_ToolTip;

   this.info_TextBox = new TextBox( this );
   this.info_TextBox.toolTip = info_ToolTip;
   this.info_TextBox.setScaledFixedHeight( 80 );

   this.infoClear_Button = new ToolButton( this );
   this.infoClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.infoClear_Button.setScaledFixedSize( 22, 22 );
   this.infoClear_Button.focusStyle = FocusStyle_NoFocus;
   this.infoClear_Button.toolTip = "<p>Clear the Information field.</p>";
   this.infoClear_Button.onClick = function()
   {
      g_workingData.info = this.dialog.info_TextBox.text = "";
      this.dialog.info_TextBox.hasFocus = true;
   };

   this.info_Sizer = new HorizontalSizer;
   this.info_Sizer.spacing = 4;
   this.info_Sizer.add( this.info_Label );
   this.info_Sizer.add( this.info_TextBox, 100 );
   this.info_Sizer.add( this.infoClear_Button );

   //

   this.reset_Button = new PushButton( this );
   this.reset_Button.text = "Reset";
   this.reset_Button.icon = this.scaledResource( ":/icons/reload.png" );
   this.reset_Button.onClick = function()
   {
      g_workingData.reset();
      g_workingData.saveSettings();
      this.dialog.updateControls();
   };

   this.submit_Button = new PushButton( this );
   this.submit_Button.defaultButton = true;
   this.submit_Button.text = "Submit";
   this.submit_Button.icon = this.scaledResource( ":/icons/upload.png" );
   this.submit_Button.onClick = function()
   {
      this.dialog.ok();
   };

   this.exit_Button = new PushButton( this );
   this.exit_Button.text = "Exit";
   this.exit_Button.icon = this.scaledResource( ":/icons/close.png" );
   this.exit_Button.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.spacing = 8;
   this.buttons_Sizer.add( this.reset_Button );
   this.buttons_Sizer.addStretch();
   this.buttons_Sizer.add( this.submit_Button );
   this.buttons_Sizer.add( this.exit_Button );

   //

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;
   this.sizer.add( this.helpLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.useKeysFile_Sizer );
   this.sizer.add( this.keysFile_Sizer );
   this.sizer.add( this.password_Sizer );
   this.sizer.add( this.useManualData_Sizer );
   this.sizer.add( this.developerId_Sizer );
   this.sizer.add( this.publicKeyHex_Sizer );
   this.sizer.add( this.contactEmail_Sizer );
   this.sizer.add( this.publicEmail_Sizer );
   this.sizer.add( this.url_Sizer );
   this.sizer.add( this.name_Sizer, 10 );
   this.sizer.add( this.info_Sizer, 10 );
   this.sizer.add( this.buttons_Sizer );

   this.windowTitle = TITLE;

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinWidth();
   this.setFixedHeight();

   this.onReturn = function( retVal )
   {
      if ( retVal == StdDialogCode_Ok )
      {
         g_workingData.useKeysFile = this.useKeysFile_CheckBox.checked;
         g_workingData.keysFile = this.keysFile_Edit.text.trim();
         g_workingData.developerId = this.developerId_Edit.text.trim();
         g_workingData.publicKeyHex = this.publicKeyHex_Edit.text.trim();
         g_workingData.contactEmail = this.contactEmail_Edit.text.trim();
         g_workingData.publicEmail = this.publicEmail_Edit.text.trim();
         g_workingData.url = this.url_Edit.text.trim();
         g_workingData.name = this.name_TextBox.text.trim();
         g_workingData.info = this.info_TextBox.text.trim();
      }
   };

   this.updateControls = function()
   {
      this.useKeysFile_CheckBox.checked = g_workingData.useKeysFile;
      this.keysFile_Edit.text = g_workingData.keysFile;
      this.useManualData_CheckBox.checked = !g_workingData.useKeysFile;
      this.developerId_Edit.text = g_workingData.developerId;
      this.publicKeyHex_Edit.text = g_workingData.publicKeyHex;
      this.contactEmail_Edit.text = g_workingData.contactEmail;
      this.publicEmail_Edit.text = g_workingData.publicEmail;
      this.url_Edit.text = g_workingData.url;
      this.name_TextBox.text = g_workingData.name;
      this.info_TextBox.text = g_workingData.info;

      this.keysFile_Label.enabled = g_workingData.useKeysFile;
      this.keysFile_Edit.enabled = g_workingData.useKeysFile;
      this.keysFileSelect_Button.enabled = g_workingData.useKeysFile;
      this.password_Label.enabled = g_workingData.useKeysFile;
      this.password_Edit.enabled = g_workingData.useKeysFile;
      this.passwordClear_Button.enabled = g_workingData.useKeysFile;
      this.passwordHide_Button.enabled = g_workingData.useKeysFile;

      this.developerId_Label.enabled = !g_workingData.useKeysFile;
      this.developerId_Edit.enabled = !g_workingData.useKeysFile;
      this.developerIdClear_Button.enabled = !g_workingData.useKeysFile;
      this.publicKeyHex_Label.enabled = !g_workingData.useKeysFile;
      this.publicKeyHex_Edit.enabled = !g_workingData.useKeysFile;
      this.publicKeyHexClear_Button.enabled = !g_workingData.useKeysFile;
   };

   this.updateControls();
}

SubmitCPDDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
// EOF SubmitCPDGUI.js - Released 2022-04-13T18:28:34Z
