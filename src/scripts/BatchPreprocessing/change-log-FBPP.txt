===============================================================================
FastBatchPreprocessing Script Changelog
===============================================================================

2024/08/31 - v1.1.2
-------------------------------------------------------------------------------

- Improvement: master calibration files are checked to be saved in xisf format,
  any other file format for a calibration master files will be rejected and a 
  proper warning is reported.


2024/08/10 - v1.1.1
-------------------------------------------------------------------------------

- Bugfix: Fixed wrong tool tip for the 'High sigma' control. Replaced
  'increase' by 'decrease' because the lower the high sigma value, the more hot
  pixels can be selected by the automatic cosmetic correction feature.

2024/08/02 - v1.1.0
-------------------------------------------------------------------------------

- Improvement: Cosmetic correction has been extended to use the implementation
  available in the ImageCalibration process.

- Improvement: Weighting options added in post-calibration panel.

- Improvement: Drizzle settings added in post-calibration panel.

- Improvement: A new button to load the session in WBPP has been added.

- Improvement: Astrometric Solution checkbox added under the light panel.

- Improved tooltips and log messages.

- Code cleanup.

2024/06/28 - v1.0.2
-------------------------------------------------------------------------------
- Bugfix: Set the parameter 'tryApparentCoordinates = true' when initializing
  ImageSolver parameters.

- PixInsight core version >= 1.8.9-3 required.

2024/06/20 - v1.0.1
-------------------------------------------------------------------------------

- Bugfix: SNR evaluation was enabled for all integrated masters, now it is
  enabled only for the integrated master lights.

2024/06/19 - v1.0
-------------------------------------------------------------------------------

- Initial implementation with essential GUI.
