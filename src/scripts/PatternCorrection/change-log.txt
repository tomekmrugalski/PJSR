===============================================================================
Linear Defect Correction Scripts Changelog
===============================================================================

1.02 - September 7 2022

   - The scripts are now featured in a dedicated Pattern Correction section
     under the Script main menu item.

   - Bugfix: custom postfix value was not set before executing LPS engine.

   - PixInsight core 1.8.8-8 requirement check updated.

-------------------------------------------------------------------------------
1.01 - April 11 2021

   - Code reorganization to use the new pjsr/LinearDefectDetection.jsh and
     pjsr/LinearPatternSubtraction.jsh standard platform include files in
     PixInsight 1.8.8-8.

   - PixInsight core 1.8.8-8 required.

-------------------------------------------------------------------------------
1.0 - December 2019

   - First public release.
