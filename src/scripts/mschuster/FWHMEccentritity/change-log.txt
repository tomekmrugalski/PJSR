===============================================================================
FWHMEccentricity Script Changelog
===============================================================================

-------------------------------------------------------------------------------
2022.05.11 - v1.5.2

- Star detection: default parameter values and interface controls adapted to
  the new StarDetector V2 engine, including parameter ranges and tool tips.

- PixInsight core >= 1.8.9-1 required.

-------------------------------------------------------------------------------
2021.05.17 - v1.5.1

- New SVG icon.

- PixInsight core >= 1.8.8-8 required.

-------------------------------------------------------------------------------
2015.11.24 - v1.5

- Improved high-res monitor support.

-------------------------------------------------------------------------------
2015.08.09 - v1.4

- Improved high-res monitor support.

-------------------------------------------------------------------------------
2015.08.06 - v1.3

- Improved high-res monitor support.

- Updated tool tips and copyright.

-------------------------------------------------------------------------------
2014.08.07 - v0.14

- Add explicit contour plot font specification.

- Use static Measure button text.

-------------------------------------------------------------------------------
2013.09.02 - v0.12

- Initial public release.
-------------------------------------------------------------------------------
