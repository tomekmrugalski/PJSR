// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// Test-VaryParams-unit-test-support.js
// ----------------------------------------------------------------------------
//
// Copyright (c) 2012-2022 Jean-Marc Lugrin.
// Copyright (c) 2003-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------
"use strict";


// Exercise (somewhat) the test environment itself
// The test workspace must be loaded !

// Additional log
//#define DEBUG true

// for vP_isOpenWindow
#include "../../../scripts/VaryParams/VaryParams-helpers.jsh"

#include "VaryParams-unit-tests-support.jsh"



// Expected environment (views and processes)
#define VP_TEST_STRETCHED "TestStarFieldStretched_L"
#define VP_TEST_LINEAR "TestStarFieldLinear_L"
var test_requiredViews = [VP_TEST_STRETCHED,VP_TEST_LINEAR];

var test_requiredProcesses = ["anACDNR","aProcessContainer","aStarMask","aSecondStarMask","aNewImage","aDeconvolution",
   "aDBE", "aPixelMathDiv2"];



Console.show();

try {

   Console.writeln("Check environment for tests, close non required windows and previews");
   vP_checkPresenceOfViews(test_requiredViews);
   vP_closeNonTestWindows(test_requiredViews);
   vP_closePreviewsOfTestWindows(test_requiredViews);

   vP_checkPresenceOfProcesses(test_requiredProcesses);

   // TODO Some checks of assert function

   Console.writeln("Completed");
} catch(x)
{
   Console.criticalln("Test failed: " + x);
}


